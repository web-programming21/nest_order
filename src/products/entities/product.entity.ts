import { OrderItem } from 'src/orders/entities/order-item';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  CreateDateColumn,
  DeleteDateColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity()
export class Product {
  @PrimaryGeneratedColumn()
  id: number;
  @Column({ length: 62 })
  name: string;
  @Column({ type: 'float' })
  price: number;
  @OneToMany(() => OrderItem, (orderItems) => orderItems.product)
  orderItems: OrderItem[];
  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;
  @DeleteDateColumn()
  deleteDate: Date;
}
